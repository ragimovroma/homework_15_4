﻿#include <iostream>

int main()
{
	setlocale(0, "");
	
	for (int n = 0; n < 15; ++n)
	{
		if (n % 2 == 0)
		{
			std::cout << "Чётное число :"<< n << '\n';
		}
		else if(n % 2 == 1)
		{
			std::cout << "Нечётное число :" << n << '\n';
		}
	}
}